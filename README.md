# MAISEN Core v0.1

## Running

#### 1. Setup environment

The software was developed with **Python 3.5**. Please sure your environment has all the required packages installed - you can find them in requirements.txt file.
    
To install with PIP:

`pip install -r requirements.txt`

#### 2. Configs

Fill config files in 'configs' folder with valid information.

#### 3. Running

The options are:

`-i` input file - only fasta files are allowed (.fasta .fna extension only)

`-o` output directory

example:

`python3 maisen_struct.py -i myfasta.fasta -o results_dir`

## Authors

* Mikołaj Dziurzyński ([@mdziurzynski](https://github.com/mdziurzynski))
* Przemysław Decewicz ([@pdec](https://github.com/pdec))