# -*- coding: utf-8 -*-
from os import path
from contextlib import contextmanager

from Bio import SeqIO

__author__ = u"Mikołaj Dziurzyński"


def fasta_container_helper(cwd, main_fasta_path):
    single_fastas_list = []
    fasta_ids = []
    try:
#        if main_fasta_path.split('.')[-1] not in ['fasta', 'fna', 'fa','fas']:
#            raise ValueError('Given file does not seem to be of FASTA format. Allowed formats: .fasta .fna')
        for single_fasta in SeqIO.parse(main_fasta_path, 'fasta'):
            if single_fasta.id in fasta_ids:
                raise IOError("Given fasta file contains at least 2 sequences with the same ID {0}.".format(single_fasta.id))
            if not str(single_fasta.seq):
                continue

            fasta_ids.append(single_fasta.id)

            abs_path = path.join(cwd, "tmp/single_fastas/{0}.fasta".format(single_fasta.id))
            SeqIO.write(single_fasta, abs_path, 'fasta')
            single_fastas_list.append(FastaContainer(abs_path))
    except FileNotFoundError:
        raise FileNotFoundError("Given fasta file does not exist!")

    main_fasta = FastaContainer(main_fasta_path)

    return main_fasta, single_fastas_list


class FastaContainer:

    def __init__(self, fasta_path):
        self.name = path.basename(fasta_path)
        self.abs_path = fasta_path

    @contextmanager
    def get_seqio_record(self):
        try:
            record = SeqIO.read(self.abs_path, 'fasta')
            yield record
            del record
        except ValueError:
            raise ValueError("You are trying to read multiple fasta file into SeqIO record - it is not possible.")
