# -*- coding: utf-8 -*-
from os import makedirs, path
from shutil import rmtree

__author__ = u"Mikołaj Dziurzyński"


def create_folder_hierarchy(main_path, programs, output_dir=False):
    try:
        makedirs(path.join(main_path, "tmp/single_fastas"))
        makedirs(path.join(main_path, "tmp/outputs/intermediate_gbk"))
        makedirs(path.join(main_path, "tmp/gff_dbs"))
        makedirs(path.join(main_path, "tmp/program_logs"))
        for program in programs:
            makedirs(path.join(main_path, "tmp", program))
            makedirs(path.join(main_path, "tmp/outputs", program))
            makedirs(path.join(main_path, "tmp/intermediate_gbk", program))

        if output_dir:
            if output_dir.startswith("/"):
                makedirs(output_dir)
            else:
                makedirs(path.join(main_path, output_dir))

    except PermissionError:
        raise PermissionError("Cannot create folder in main directory. Make sure you have appropriate rights")

    except FileExistsError:
        raise FileExistsError("Please make sure that all your configs have unique values in 'program' variable.")


def delete_folder_hierarchy(main_path, stage):
    try:
        rmtree(path.join(main_path, 'tmp'))
    except FileNotFoundError:
        if stage == "opening":
            print("Cleaning up before startup...\ndone.\n")
        elif stage == 'closing':
            raise FileNotFoundError("'tmp' folder does not exist - did you delete it? This folder holds temporary program data. It will be deleted when the program finish.")
