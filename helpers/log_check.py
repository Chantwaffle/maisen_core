from os import listdir
import mmap


def check_logs_for_errors(cwd):
    abs_log_dirs = cwd + "/tmp/program_logs/"
    for log_file in listdir(abs_log_dirs):
        with open(abs_log_dirs + log_file, 'rb', 0) as file, mmap.mmap(file.fileno(), 0, access=mmap.ACCESS_READ) as s:
            if s.find(b'ERROR') != -1 or s.find(b'error') != -1 or s.find(b'Error') != -1:
                return True
