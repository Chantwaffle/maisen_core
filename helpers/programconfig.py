# -*- coding: utf-8 -*-
from os import path, listdir
import subprocess

__author__ = u"Mikołaj Dziurzyński"


def program_config_helper(path_to_configs, program_type):

    try:
        cf_paths = [path.join(path_to_configs, f) for f in listdir(path_to_configs) if path.isfile(path.join(path_to_configs, f))]
    except FileNotFoundError:
        print(path_to_configs)
        raise FileNotFoundError("Cannot find configs folder.")

    configs_list = []
    for cf_path in cf_paths:
        new_config = ProgramConfig(cf_path, program_type)
        if int(new_config.run) == 1:
            configs_list.append(new_config)

    if not configs_list:
        raise EnvironmentError("There are no programs to run for {0} annotation! Please check your configs.".format(program_type))

    return configs_list


class ProgramConfig:

    def __init__(self, config_path, program_type):
        self.name = path.basename(config_path)
        self.type = program_type
        self.config_path = config_path
        with open(config_path, "r") as f:
            for line in f.readlines():
                if not line.startswith("#") and line not in ["", "\n"]:
                    setattr(self, line.split("=")[0].strip(), line.split("=")[-1].strip())

        self.check()

    def check(self):
        # emit warning if program is flagged not to run
        if not int(self.run):
            return "Program {0} will not be run - to run it, edit its config.".format(self.name)
        else:
            # if program exists
            if not path.isfile(self.path):
                raise FileNotFoundError('Run file for {0.name} ({0.path}) does not exist!'.format(self))
            # if run_command is available
            res_code = subprocess.getstatusoutput('{0.run_command} none'.format(self))[0]
            if res_code == 127:
                raise ValueError("{0.name} - Command '{0.run_command}' is not available!".format(self))
            # if *output* and *input* are present in run_options
            if not ('*input*' in self.run_options):
                raise ValueError("{0.name} - run_options parameter does not contain *input* flag! Please correct this config.".format(self))
            elif not ('*output*' in self.run_options):
                raise ValueError("{0.name} - run_options parameter does not contain *output* flag! Please correct this config.".format(self))
            # if output_format is correct
            if self.output_format != 'gff':
                raise ValueError("{0.name} - '{0.output_format}' is not a valid output format! Please correct this config.".format(self))
            if self.program not in ['genemarks', 'prodigal', 'other']:
                raise ValueError("{0.name} - wrong 'program' option in this config.".format(self))
        return True
