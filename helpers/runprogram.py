
# -*- coding: utf-8 -*-
from multiprocessing import Pool, cpu_count
import subprocess
from os import path, makedirs, chdir, getcwd, system
from uuid import uuid4


def runner(program_name, run_command, program_path, run_details):
    # cpu_to use_count = multiprocessing.cpu_count() -1
    try:
        entry_cwd = getcwd()

        run_path = path.join("tmp/{0}/{1}".format(program_name, run_details['name']))
        makedirs(run_path)
        chdir(run_path)

        main_run_command = "{0} {1}".format(
                program_path, run_details['options'])

        if run_command in ['', './']:
            subprocess.check_call(main_run_command, shell=True, executable="/bin/bash")
        else:
            subprocess.check_call("{0} ".format(
                run_command) + main_run_command, shell=True, executable="/bin/bash")
        chdir(entry_cwd)

    except:
        print("Got error from program {0}".format(path.basename(program_path)))
        raise

    return True


def run_program(config, input_list, cwd):
    run_options_list = []
    for record in input_list:
        output_path = path.join(cwd, "tmp/outputs/{0}/{1}.gff".format(config.program, record.name.split(".")[0]))

        single_run = {'name': record.name.split(".")[0]}
        options = config.run_options.replace('*input*', '{0}').replace('*output*', '{1}')
        single_run["options"] = options.format(record.abs_path, output_path)
        run_options_list.append(single_run)

    numer_of_workers = cpu_count() - 1

    with Pool(processes=numer_of_workers) as pool:
        multiple_results = [pool.apply_async(
            runner, (config.program, config.run_command, config.path, single_run_details ))
            for single_run_details in run_options_list]

        [res.get() for res in multiple_results]

    return True
