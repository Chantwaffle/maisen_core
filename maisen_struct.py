# -*- coding: utf-8 -*-
import argparse
from os import getcwd
import sys

from maisen_core.helpers.programconfig import program_config_helper
from maisen_core.helpers.fastacontainer import fasta_container_helper
from maisen_core.helpers.foldermanagement import create_folder_hierarchy, delete_folder_hierarchy
from maisen_core.helpers.runprogram import run_program
from maisen_core.helpers.gffparsing import create_gbk_output
from maisen_core.helpers.log_check import check_logs_for_errors

__author__ = u"Mikołaj Dziurzyński"


# 1) read fasta or fna files
# 1a) check if it's fasta
# 2) break input into single sequences
# 3) run genemarks on these single sequences - use multiprocessing
# 4) parse output -> fasta + output = gbk


def maisen_struct_run(fasta_path, output_dir, configs_dir=None):
    "Main Maisen Core function"

    cwd = getcwd()

    # remove tmp folder if it exists (may remain if the program crashed)
    delete_folder_hierarchy(cwd, "opening")

    # get structural analysis programs configs
    print("Loading structural configs...")

    configs_dir = configs_dir and configs_dir or getcwd() + '/configs'

    struct_configs_list = program_config_helper(configs_dir + '/structural', "structural")
    print("done.\n")

    # create folder hierarchy
    # TODO check if output dir exists
    programs = []
    for config in struct_configs_list:
        programs.append(config.program)
    print("Creating folder hierarchies...")
    create_folder_hierarchy(cwd, programs, output_dir)
    print("done.\n")

    # load input file and split it into list of dicts
    print("Loading input data...")
    main_fasta, single_fastas_list = fasta_container_helper(cwd, fasta_path)
    print("done.\n")

    # run programs and collect output
    # structural programs
    print("Running structural annotation programs:")
    for config in struct_configs_list:
        if not int(config.run):
            print("Skipping program {0}... (to run, set 'run = 1' in config)\n".format(config.name.split('.')[0]))
            continue
        print("Running {0}...".format(config.name.split('.')[0]))
        if config.program == 'prodigial':
            # if not anonymous mode then use main fasta
            if not ('-p meta' in config.run_options or '--mode meta' in config.run_options):
                run_program(config, [main_fasta], cwd)
                continue

        run_program(config, single_fastas_list, cwd)
        print("Finished running {0}\n".format(config.name))

    # TODO functional programs

    # integrate input and output into gbk file
    print("Merging outputs from structural annotation programs...\n")
    create_gbk_output(programs, output_dir, cwd)
    print("done.\n")

    if check_logs_for_errors(cwd):
        raise Exception("Error during struct program running!")
    # cleanup
    # delete_folder_hierarchy(cwd, "closing")

    print("MAISE Core finished running! Your output is available at '{0}'\n".format(output_dir))


if __name__ == "__main__":
    print("MAISEN Core v0.1\nhttps://github.com/mdziurzynski/maisen_core\n\n")

    parser = argparse.ArgumentParser(description='MAISEN core')
    parser.add_argument('-i', type=str, help='path to fasta file')
    parser.add_argument('-o', type=str, help='path to output directory')
    parser.add_argument('-c', type=str, help='path to configs directory')

    args = parser.parse_args()
    if not args.i or not args.o:
        parser.print_help()
        sys.exit(1)

    print("File to process: {0}\n\n".format(args.i))
    maisen_struct_run(args.i, args.o, args.c)
