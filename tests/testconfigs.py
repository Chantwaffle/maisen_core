import unittest
from os import getcwd, path, makedirs
from shutil import rmtree

from maisen_core.helpers.programconfig import program_config_helper, ProgramConfig


class TestConfigs(unittest.TestCase):

    def test_config_wrong_path(self):
        path = "not/a/real/path"
        with self.assertRaises(FileNotFoundError):
            try:
                program_config_helper(path, 'not_tested')
            except FileNotFoundError as e:
                self.assertEqual(e.args[0], "Cannot find configs folder.")
                raise

    def test_no_run(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy1_no_run.config")
        config = ProgramConfig(test_conf_path, 'structural')
        res = config.check()
        self.assertEqual(res, "Program {0} will not be run - to run it, edit its config.".format(config.name))

    def test_no_program(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy2_no_program.config")
        with self.assertRaises(FileNotFoundError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except FileNotFoundError as e:
                self.assertEqual(e.args[0], "Run file for dummy2_no_program.config (/noprog.pl) does not exist!")
                raise

    def test_no_run_command(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy3_no_command.config")
        with self.assertRaises(ValueError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except ValueError as e:
                self.assertEqual(e.args[0], "dummy3_no_command.config - Command 'not_a_command' is not available!")
                raise

    def test_no_input_flag(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy4_no_input.config")
        with self.assertRaises(ValueError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except ValueError as e:
                self.assertEqual(e.args[0], "dummy4_no_input.config - run_options parameter does not contain *input* flag! Please correct this config.")
                raise

    def test_no_output_flag(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy5_no_output.config")
        with self.assertRaises(ValueError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except ValueError as e:
                self.assertEqual(e.args[0], "dummy5_no_output.config - run_options parameter does not contain *output* flag! Please correct this config.")
                raise

    def test_wrong_output_format(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy6_wrong_output_format.config")
        with self.assertRaises(ValueError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except ValueError as e:
                self.assertEqual(e.args[0], "dummy6_wrong_output_format.config - 'not_a_valid_format' is not a valid output format! Please correct this config.")
                raise

    def test_wrong_program_option(self):
        test_conf_path = path.join(getcwd(), "tests/configs/structural/dummy7_wrong_program_option.config")
        with self.assertRaises(ValueError):
            try:
                ProgramConfig(test_conf_path, 'structural')
            except ValueError as e:
                self.assertEqual(e.args[0], "dummy7_wrong_program_option.config - wrong 'program' option in this config.")
                raise

    def test_no_struct_program(self):
        test_conf_path = path.join(getcwd(), "tests/struct_no_run")
        makedirs(test_conf_path)
        with self.assertRaises(EnvironmentError):
            try:
                program_config_helper(test_conf_path, 'structural')
            except EnvironmentError as e:
                self.assertEqual(e.args[0], "There are no programs to run for structural annotation! Please check your configs.")
                rmtree(test_conf_path)
                raise

    # add tests for correct runs
