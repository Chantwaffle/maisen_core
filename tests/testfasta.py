import unittest
from os import getcwd, path

from maisen_core.helpers.fastacontainer import fasta_container_helper, FastaContainer
from maisen_core.helpers.foldermanagement import create_folder_hierarchy, delete_folder_hierarchy


class TestFasta(unittest.TestCase):

    def __init__(self, *args):
        super().__init__(*args)
        self.cwd = getcwd()

    def setUp(self):
        cwd = getcwd()
        create_folder_hierarchy(cwd, [])

    def tearDown(self):
        cwd = getcwd()
        delete_folder_hierarchy(cwd, stage="closing")

    def test_file_doesnt_exist(self):
        file_path = "not/a/real/path.fasta"
        with self.assertRaises(FileNotFoundError):
            try:
                fasta_container_helper(self.cwd, file_path)
            except FileNotFoundError as e:
                self.assertEqual(e.args[0], "Given fasta file does not exist!")
                raise

    def test_not_a_fasta(self):
        file_path = "tests/testing_data/SCU49845_test1.gbk"
        with self.assertRaises(ValueError):
            try:
                fasta_container_helper(self.cwd, file_path)
            except ValueError as e:
                self.assertEqual(e.args[0], "Given file does not seem to be of FASTA format. Allowed formats: .fasta .fna")
                raise

    def test_main_fasta_created(self):
        file_path = path.join(self.cwd, "tests/testing_data/454Scaffolds_test.fna")
        main_fasta, single_fastas_list = fasta_container_helper(self.cwd, file_path)
        self.assertEqual(main_fasta.abs_path, file_path)

    def test_single_fasta_created_with_correct_names(self):
        file_path = path.join(self.cwd, "tests/testing_data/454Scaffolds_test.fna")
        main_fasta, single_fastas_list = fasta_container_helper(self.cwd, file_path)
        testing_fasta_names = ['scaffold00001', 'scaffold00002', 'scaffold00003', 'scaffold00004', 'scaffold00005', 'scaffold00006', 'scaffold00007', 'scaffold00008', 'scaffold00009', 'scaffold00010', 'scaffold00011', 'scaffold00012', 'scaffold00013', 'scaffold00014', 'scaffold00015', 'scaffold00016', 'scaffold00017', 'scaffold00018', 'scaffold00019', 'scaffold00020', 'scaffold00021', 'scaffold00022', 'scaffold00023', 'scaffold00024', 'scaffold00025', 'scaffold00026', 'scaffold00027', 'scaffold00028', 'scaffold00029', 'scaffold00030', 'scaffold00031', 'scaffold00032', 'scaffold00033', 'scaffold00034', 'scaffold00035', 'scaffold00036', 'scaffold00037', 'scaffold00038', 'scaffold00039', 'scaffold00040', 'scaffold00041', 'scaffold00042', 'scaffold00043', 'scaffold00044', 'scaffold00045', 'scaffold00046', 'scaffold00047', 'scaffold00048', 'scaffold00049', 'scaffold00050', 'scaffold00051', 'scaffold00052', 'scaffold00053', 'scaffold00054', 'scaffold00055', 'scaffold00056', 'scaffold00057', 'scaffold00058', 'scaffold00059', 'scaffold00060', 'scaffold00061', 'scaffold00062', 'scaffold00063', 'scaffold00064', 'scaffold00065', 'scaffold00066', 'scaffold00067', 'scaffold00068', 'scaffold00069', 'scaffold00070', 'scaffold00071', 'scaffold00072', 'scaffold00073', 'scaffold00074', 'scaffold00075', 'scaffold00076', 'scaffold00077', 'scaffold00078']

        list_to_compare = []
        for fasta in single_fastas_list:
            with fasta.get_seqio_record() as record:
                list_to_compare.append(record.name)

        self.assertEqual(testing_fasta_names, sorted(list_to_compare))

    def test_fasta_read_multiple_fasta_in_seqio(self):

        file_path = path.join(self.cwd, "tests/testing_data/454Scaffolds_test.fna")
        main_fasta, single_fastas_list = fasta_container_helper(self.cwd, file_path)
        with self.assertRaises(ValueError):
            try:
                with main_fasta.get_seqio_record():
                    pass
            except ValueError as e:
                self.assertEqual(e.args[0], "You are trying to read multiple fasta file into SeqIO record - it is not possible.")
                raise
    # add test to test correct runs
