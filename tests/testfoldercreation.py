import unittest
from os import getcwd, path
from shutil import rmtree

from maisen_core.helpers.foldermanagement import create_folder_hierarchy, delete_folder_hierarchy


class TestHierarchyCreation(unittest.TestCase):

    def test_cannot_create_folders(self):
        folder_path = "/maisentest"
        with self.assertRaises(PermissionError):
            try:
                create_folder_hierarchy(folder_path, [])
            except PermissionError as e:
                self.assertEqual(e.args[0], "Cannot create folder in main directory. Make sure you have appropriate rights")
                raise

    def test_cannot_delete_folder_does_not_exist(self):
        folder_path = path.join(getcwd(), "tests")
        self.assertEqual(False, path.exists(path.join(getcwd(), "tests/tmp")))

        with self.assertRaises(FileNotFoundError):
            try:
                delete_folder_hierarchy(folder_path, stage="closing")
            except FileNotFoundError as e:
                self.assertEqual(e.args[0], "'tmp' folder does not exist - did you delete it? This folder holds temporary program data. It will be deleted when the program finish.")
                raise

    def test_try_delete_nonexisting_folder_during_startup(self):
        """When given 'opening' parameter, the function wont raise error"""
        folder_path = path.join(getcwd(), "tests")

        self.assertEqual(False, path.exists(path.join(getcwd(), "tests/tmp")))

        delete_folder_hierarchy(folder_path, stage="opening")
        self.assertEqual(False, path.exists(path.join(getcwd(), "tests/tmp")))

    def test_try_multiple_programs(self):
        folder_path = path.join(getcwd(), "tests/test_try_multiple_programs")

        if path.isfile(folder_path):
            rmtree(folder_path)

        with self.assertRaises(FileExistsError):
            try:
                create_folder_hierarchy(folder_path, ['double', 'double'])
            except FileExistsError as e:
                rmtree(folder_path)
                self.assertEqual(e.args[0], "Please make sure that all your configs have unique values in 'program' variable.")
                raise
